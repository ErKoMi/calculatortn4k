﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace calc

{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            bt_div.Tag = calculate.Operations.div; // тегу кнопки привязываем математическую операцию
            bt_mul.Tag = calculate.Operations.mul;
            bt_sub.Tag = calculate.Operations.sub;
            bt_sum.Tag = calculate.Operations.sum;
            bt_sqr.Tag = calculate.Operations.sqr;
            bt_1delx.Tag = calculate.Operations.fr;
            bt_sqrt.Tag = calculate.Operations.sqrt;
            this.Width = 340;
        }

        bool f_znak = false; // флаг ввода нового числа
        bool f_ravno = false; // флаг сброса
        bool change = false; // флаг изменения числа
        calculate CALLKLKLKLKLLLKLKLKL = new calculate(); // экземпляр класса Калькулятор
        string error = "Ошибка";

        private void Operation(object sender, EventArgs e) // обработчик нажатия кнопки математической операции
        {
           InputOperation((calculate.Operations)(sender as  Button).Tag);
        }

        /// <summary>
        /// Ввод математической операции
        /// </summary>
        /// <param name="sum"></param>
        private void InputOperation(calculate.Operations operation)
        {
            try
            {
                f_znak = true;
                calculate.Operations op = operation; // получаем операцию с тега кнопки
                Output.Text = CALLKLKLKLKLLLKLKLKL.Inp_op(double.Parse(Output.Text), op, change); // отдаём: текущее значение на табло, операцию (строка выше), флаг изменения числа
                change = false; // т.к. число не было изменено
            }
            catch (Exception)
            {
                Output.Text = error;
            }
        }


        private void Input(string st) // вывод на табло
        {
            if (!f_ravno)  // если не стоит знак сброса
            {
                if (Output.Text.Length >= 8) return;
                if (Output.Text == "0" || Output.Text == "-0")
                {
                    Output.Text = "";
                }
                Output.Text += st;
            }
            else  // сбрасываем табло
            {
                f_ravno = false;
                Output.Text = "";
                Output.Text = st;
            }
        }

        /// <summary>
        /// Нажатие цифровой кнопки на форме
        /// </summary>
        private void Digit_Click(object sender, EventArgs e) // ввод цифры
        {

            InputDigit((sender as Button).Text);
        }

        /// <summary>
        /// Ввод числа
        /// </summary>
        private void InputDigit(string text)
        {
            if (Output.Text == error)
            {
                Clear();
            }
            if (f_znak)
            {
                Output.Text = "0"; f_znak = false;
            }
            Input(text); // вывод на экран
            change = true;
        }

        private void ravno_Click(object sender, EventArgs e)  // нажатие кнопки "равно"
        {
            if (Output.Text == error)
            {
                Clear();
            }
            f_ravno = true;

            try
            {
                Output.Text = CALLKLKLKLKLLLKLKLKL.Out(double.Parse(Output.Text));
                change = false;
            }
            catch (Exception)
            {
                Output.Text = error;
            }

            history_list.Items.Add(Output.Text);


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button19_Click(object sender, EventArgs e) //стереть все
        {
            label1.Text = "";
            Output.Text = "0";
            CALLKLKLKLKLLLKLKLKL.Sbros();
            f_ravno = false;
            change = false;
        }

        private void button18_Click(object sender, EventArgs e) //стирание последней цифры
        {
            Backspace();
        }


        private void point_Click(object sender, EventArgs e) // запятая
        {
            InputSeparator();
        }

        private void InputSeparator()
        {
            if (f_ravno) { Clear(); }
            if (f_znak) { Output.Text = "0"; f_znak = false; }
            if (Output.Text == error) { Clear(); }
            if (Output.Text.Contains(',')) return;
            Output.Text += ",";
            change = true;
        }

        private void hz_Click(object sender, EventArgs e) // изменить знак
        {

            if (Output.Text == error) { Clear(); }
            if (Output.Text[0] == '-')
                Output.Text = Output.Text.Substring(1);
            else
                Output.Text = "-" + Output.Text;
            if (f_ravno)
            {
                CALLKLKLKLKLLLKLKLKL.ChangeSign();
            }

            change = false;
        }

        private void Clear() // сброс
        {
            CALLKLKLKLKLLLKLKLKL.Sbros();
            Output.Text = "0";
            f_znak = false;
        }

        /// <summary>
        /// Нажатие клавиши клавиатуры
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                InputDigit(e.KeyChar.ToString());
            }

            if (e.KeyChar == '+')
            {
                InputOperation(calculate.Operations.sum);
            }
            if (e.KeyChar == '-')
            {
                InputOperation(calculate.Operations.sub);
            }
            if (e.KeyChar == '*')
            {
                InputOperation(calculate.Operations.mul);
            }
            if (e.KeyChar == '/')
            {
                InputOperation(calculate.Operations.div);
            }

            if (e.KeyChar == ',')
            {
                InputSeparator();
            }   

            if (e.KeyChar == (char)Keys.Back)
            {
                Backspace();
            }

        }

        private void Backspace()
        {
            if (Output.Text == error)
            {
                Clear();
            }
            else
            {
                if (!f_ravno)
                {
                    Output.Text = Output.Text.Substring(0, Output.Text.Length - 1);  // убираем последний символ
                    if (Output.Text == "" || Output.Text == "-") Output.Text = "0";
                }
            }
        }

        private void обычныйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Width > 340)
            {
                this.Width = 340;
            }
        }

        private void расширенныйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Width < 904)
            {
                this.Width = 904;
            }
        }

        private void Output_DragDrop(object sender, DragEventArgs e)
        {
            Clear();
            Output.Text = e.Data.GetData(typeof(string)).ToString();
            change = true;
        }

        private void Output_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void history_list_MouseDown(object sender, MouseEventArgs e)
        {
            ListBox h = (ListBox)sender;
            if (h.SelectedItem == null) return;
            h.DoDragDrop(h.SelectedItem, DragDropEffects.Copy);
        }

        private void history_list_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void history_list_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void clear_history_Click(object sender, EventArgs e)
        {
            history_list.Items.Clear();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 a = new AboutBox1();
            a.ShowDialog();
        }
    }
}
