﻿
namespace calc
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.Output = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.hz = new System.Windows.Forms.Button();
            this.bt_sum = new System.Windows.Forms.Button();
            this.bt_sub = new System.Windows.Forms.Button();
            this.bt_mul = new System.Windows.Forms.Button();
            this.bt_div = new System.Windows.Forms.Button();
            this.point = new System.Windows.Forms.Button();
            this.ravno = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.bt_sqr = new System.Windows.Forms.Button();
            this.bt_1delx = new System.Windows.Forms.Button();
            this.bt_sqrt = new System.Windows.Forms.Button();
            this.history_list = new System.Windows.Forms.ListBox();
            this.clear_history = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.режимToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обычныйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.расширенныйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.Window;
            this.label1.Location = new System.Drawing.Point(29, 47);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(372, 39);
            this.label1.TabIndex = 0;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Output
            // 
            this.Output.AllowDrop = true;
            this.Output.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Output.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Output.ForeColor = System.Drawing.SystemColors.Window;
            this.Output.Location = new System.Drawing.Point(29, 97);
            this.Output.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(372, 46);
            this.Output.TabIndex = 0;
            this.Output.Text = "0";
            this.Output.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Output.DragDrop += new System.Windows.Forms.DragEventHandler(this.Output_DragDrop);
            this.Output.DragOver += new System.Windows.Forms.DragEventHandler(this.Output_DragOver);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button1.ForeColor = System.Drawing.SystemColors.Window;
            this.button1.Location = new System.Drawing.Point(29, 402);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 57);
            this.button1.TabIndex = 2;
            this.button1.TabStop = false;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button2.ForeColor = System.Drawing.SystemColors.Window;
            this.button2.Location = new System.Drawing.Point(132, 402);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(55, 57);
            this.button2.TabIndex = 3;
            this.button2.TabStop = false;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button3.ForeColor = System.Drawing.SystemColors.Window;
            this.button3.Location = new System.Drawing.Point(240, 402);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(55, 57);
            this.button3.TabIndex = 4;
            this.button3.TabStop = false;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button4.ForeColor = System.Drawing.SystemColors.Window;
            this.button4.Location = new System.Drawing.Point(240, 327);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(55, 57);
            this.button4.TabIndex = 7;
            this.button4.TabStop = false;
            this.button4.Text = "6";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button5.ForeColor = System.Drawing.SystemColors.Window;
            this.button5.Location = new System.Drawing.Point(132, 327);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(55, 57);
            this.button5.TabIndex = 6;
            this.button5.TabStop = false;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button6.ForeColor = System.Drawing.SystemColors.Window;
            this.button6.Location = new System.Drawing.Point(29, 327);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(55, 57);
            this.button6.TabIndex = 5;
            this.button6.TabStop = false;
            this.button6.Text = "4";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button7.ForeColor = System.Drawing.SystemColors.Window;
            this.button7.Location = new System.Drawing.Point(240, 254);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(55, 57);
            this.button7.TabIndex = 10;
            this.button7.TabStop = false;
            this.button7.Text = "9";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button8.ForeColor = System.Drawing.SystemColors.Window;
            this.button8.Location = new System.Drawing.Point(132, 254);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(55, 57);
            this.button8.TabIndex = 9;
            this.button8.TabStop = false;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button9.ForeColor = System.Drawing.SystemColors.Window;
            this.button9.Location = new System.Drawing.Point(29, 254);
            this.button9.Margin = new System.Windows.Forms.Padding(4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(55, 57);
            this.button9.TabIndex = 8;
            this.button9.TabStop = false;
            this.button9.Text = "7";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.Digit_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.button10.ForeColor = System.Drawing.SystemColors.Window;
            this.button10.Location = new System.Drawing.Point(29, 485);
            this.button10.Margin = new System.Windows.Forms.Padding(4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(157, 57);
            this.button10.TabIndex = 11;
            this.button10.TabStop = false;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.Digit_Click);
            // 
            // hz
            // 
            this.hz.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.hz.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hz.ForeColor = System.Drawing.SystemColors.Window;
            this.hz.Location = new System.Drawing.Point(240, 174);
            this.hz.Margin = new System.Windows.Forms.Padding(4);
            this.hz.Name = "hz";
            this.hz.Size = new System.Drawing.Size(55, 57);
            this.hz.TabIndex = 12;
            this.hz.TabStop = false;
            this.hz.Text = "±";
            this.hz.UseVisualStyleBackColor = false;
            this.hz.Click += new System.EventHandler(this.hz_Click);
            // 
            // bt_sum
            // 
            this.bt_sum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bt_sum.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bt_sum.ForeColor = System.Drawing.SystemColors.Window;
            this.bt_sum.Location = new System.Drawing.Point(347, 402);
            this.bt_sum.Margin = new System.Windows.Forms.Padding(4);
            this.bt_sum.Name = "bt_sum";
            this.bt_sum.Size = new System.Drawing.Size(55, 57);
            this.bt_sum.TabIndex = 13;
            this.bt_sum.TabStop = false;
            this.bt_sum.Tag = "1";
            this.bt_sum.Text = "+";
            this.bt_sum.UseVisualStyleBackColor = false;
            this.bt_sum.Click += new System.EventHandler(this.Operation);
            // 
            // bt_sub
            // 
            this.bt_sub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bt_sub.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bt_sub.ForeColor = System.Drawing.SystemColors.Window;
            this.bt_sub.Location = new System.Drawing.Point(347, 327);
            this.bt_sub.Margin = new System.Windows.Forms.Padding(4);
            this.bt_sub.Name = "bt_sub";
            this.bt_sub.Size = new System.Drawing.Size(55, 57);
            this.bt_sub.TabIndex = 14;
            this.bt_sub.TabStop = false;
            this.bt_sub.Tag = "2";
            this.bt_sub.Text = "-";
            this.bt_sub.UseVisualStyleBackColor = false;
            this.bt_sub.Click += new System.EventHandler(this.Operation);
            // 
            // bt_mul
            // 
            this.bt_mul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bt_mul.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bt_mul.ForeColor = System.Drawing.SystemColors.Window;
            this.bt_mul.Location = new System.Drawing.Point(347, 254);
            this.bt_mul.Margin = new System.Windows.Forms.Padding(4);
            this.bt_mul.Name = "bt_mul";
            this.bt_mul.Size = new System.Drawing.Size(55, 57);
            this.bt_mul.TabIndex = 15;
            this.bt_mul.TabStop = false;
            this.bt_mul.Tag = "3";
            this.bt_mul.Text = "х";
            this.bt_mul.UseVisualStyleBackColor = false;
            this.bt_mul.Click += new System.EventHandler(this.Operation);
            // 
            // bt_div
            // 
            this.bt_div.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bt_div.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bt_div.ForeColor = System.Drawing.SystemColors.Window;
            this.bt_div.Location = new System.Drawing.Point(347, 174);
            this.bt_div.Margin = new System.Windows.Forms.Padding(4);
            this.bt_div.Name = "bt_div";
            this.bt_div.Size = new System.Drawing.Size(55, 57);
            this.bt_div.TabIndex = 16;
            this.bt_div.TabStop = false;
            this.bt_div.Tag = "4";
            this.bt_div.Text = "/";
            this.bt_div.UseVisualStyleBackColor = false;
            this.bt_div.Click += new System.EventHandler(this.Operation);
            // 
            // point
            // 
            this.point.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.point.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.point.ForeColor = System.Drawing.SystemColors.Window;
            this.point.Location = new System.Drawing.Point(240, 485);
            this.point.Margin = new System.Windows.Forms.Padding(4);
            this.point.Name = "point";
            this.point.Size = new System.Drawing.Size(55, 57);
            this.point.TabIndex = 17;
            this.point.TabStop = false;
            this.point.Text = ",";
            this.point.UseVisualStyleBackColor = false;
            this.point.Click += new System.EventHandler(this.point_Click);
            // 
            // ravno
            // 
            this.ravno.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.ravno.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ravno.ForeColor = System.Drawing.SystemColors.Window;
            this.ravno.Location = new System.Drawing.Point(347, 485);
            this.ravno.Margin = new System.Windows.Forms.Padding(4);
            this.ravno.Name = "ravno";
            this.ravno.Size = new System.Drawing.Size(55, 57);
            this.ravno.TabIndex = 18;
            this.ravno.TabStop = false;
            this.ravno.Text = "=";
            this.ravno.UseVisualStyleBackColor = false;
            this.ravno.Click += new System.EventHandler(this.ravno_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button18.ForeColor = System.Drawing.SystemColors.Window;
            this.button18.Location = new System.Drawing.Point(132, 174);
            this.button18.Margin = new System.Windows.Forms.Padding(4);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(55, 57);
            this.button18.TabIndex = 19;
            this.button18.TabStop = false;
            this.button18.Text = "←";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.Firebrick;
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button19.ForeColor = System.Drawing.SystemColors.Window;
            this.button19.Location = new System.Drawing.Point(29, 174);
            this.button19.Margin = new System.Windows.Forms.Padding(4);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(55, 57);
            this.button19.TabIndex = 20;
            this.button19.TabStop = false;
            this.button19.Text = "C";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // bt_sqr
            // 
            this.bt_sqr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bt_sqr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bt_sqr.ForeColor = System.Drawing.SystemColors.Window;
            this.bt_sqr.Location = new System.Drawing.Point(447, 174);
            this.bt_sqr.Margin = new System.Windows.Forms.Padding(4);
            this.bt_sqr.Name = "bt_sqr";
            this.bt_sqr.Size = new System.Drawing.Size(55, 57);
            this.bt_sqr.TabIndex = 24;
            this.bt_sqr.TabStop = false;
            this.bt_sqr.Tag = "5";
            this.bt_sqr.Text = "x^2";
            this.bt_sqr.UseVisualStyleBackColor = false;
            this.bt_sqr.Click += new System.EventHandler(this.Operation);
            // 
            // bt_1delx
            // 
            this.bt_1delx.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bt_1delx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bt_1delx.ForeColor = System.Drawing.SystemColors.Window;
            this.bt_1delx.Location = new System.Drawing.Point(447, 254);
            this.bt_1delx.Margin = new System.Windows.Forms.Padding(4);
            this.bt_1delx.Name = "bt_1delx";
            this.bt_1delx.Size = new System.Drawing.Size(55, 57);
            this.bt_1delx.TabIndex = 23;
            this.bt_1delx.TabStop = false;
            this.bt_1delx.Tag = "6";
            this.bt_1delx.Text = "1/x";
            this.bt_1delx.UseVisualStyleBackColor = false;
            this.bt_1delx.Click += new System.EventHandler(this.Operation);
            // 
            // bt_sqrt
            // 
            this.bt_sqrt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bt_sqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bt_sqrt.ForeColor = System.Drawing.SystemColors.Window;
            this.bt_sqrt.Location = new System.Drawing.Point(447, 327);
            this.bt_sqrt.Margin = new System.Windows.Forms.Padding(4);
            this.bt_sqrt.Name = "bt_sqrt";
            this.bt_sqrt.Size = new System.Drawing.Size(55, 57);
            this.bt_sqrt.TabIndex = 22;
            this.bt_sqrt.TabStop = false;
            this.bt_sqrt.Tag = "7";
            this.bt_sqrt.Text = "√x";
            this.bt_sqrt.UseVisualStyleBackColor = false;
            this.bt_sqrt.Click += new System.EventHandler(this.Operation);
            // 
            // history_list
            // 
            this.history_list.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.history_list.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.history_list.ForeColor = System.Drawing.SystemColors.Window;
            this.history_list.FormattingEnabled = true;
            this.history_list.ItemHeight = 20;
            this.history_list.Location = new System.Drawing.Point(544, 92);
            this.history_list.Name = "history_list";
            this.history_list.Size = new System.Drawing.Size(305, 384);
            this.history_list.TabIndex = 25;
            this.history_list.SelectedIndexChanged += new System.EventHandler(this.history_list_SelectedIndexChanged);
            this.history_list.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.history_list_MouseDoubleClick);
            this.history_list.MouseDown += new System.Windows.Forms.MouseEventHandler(this.history_list_MouseDown);
            // 
            // clear_history
            // 
            this.clear_history.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.clear_history.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.clear_history.ForeColor = System.Drawing.SystemColors.Window;
            this.clear_history.Location = new System.Drawing.Point(537, 498);
            this.clear_history.Name = "clear_history";
            this.clear_history.Size = new System.Drawing.Size(311, 43);
            this.clear_history.TabIndex = 27;
            this.clear_history.Text = "Очистить";
            this.clear_history.UseVisualStyleBackColor = false;
            this.clear_history.Click += new System.EventHandler(this.clear_history_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.режимToolStripMenuItem,
            this.оПрограммеToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(886, 28);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // режимToolStripMenuItem
            // 
            this.режимToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обычныйToolStripMenuItem,
            this.расширенныйToolStripMenuItem});
            this.режимToolStripMenuItem.Name = "режимToolStripMenuItem";
            this.режимToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.режимToolStripMenuItem.Text = "Режим";
            // 
            // обычныйToolStripMenuItem
            // 
            this.обычныйToolStripMenuItem.Name = "обычныйToolStripMenuItem";
            this.обычныйToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.обычныйToolStripMenuItem.Text = "Обычный";
            this.обычныйToolStripMenuItem.Click += new System.EventHandler(this.обычныйToolStripMenuItem_Click);
            // 
            // расширенныйToolStripMenuItem
            // 
            this.расширенныйToolStripMenuItem.Name = "расширенныйToolStripMenuItem";
            this.расширенныйToolStripMenuItem.Size = new System.Drawing.Size(191, 26);
            this.расширенныйToolStripMenuItem.Text = "Расширенный";
            this.расширенныйToolStripMenuItem.Click += new System.EventHandler(this.расширенныйToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(127, 24);
            this.оПрограммеToolStripMenuItem.Text = "О программе...";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(67, 24);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.WindowText;
            this.ClientSize = new System.Drawing.Size(886, 581);
            this.Controls.Add(this.clear_history);
            this.Controls.Add(this.history_list);
            this.Controls.Add(this.bt_sqr);
            this.Controls.Add(this.bt_1delx);
            this.Controls.Add(this.bt_sqrt);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.ravno);
            this.Controls.Add(this.point);
            this.Controls.Add(this.bt_div);
            this.Controls.Add(this.bt_mul);
            this.Controls.Add(this.bt_sub);
            this.Controls.Add(this.bt_sum);
            this.Controls.Add(this.hz);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(904, 628);
            this.MinimumSize = new System.Drawing.Size(450, 628);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Калькулятор";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Output;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button hz;
        private System.Windows.Forms.Button bt_sum;
        private System.Windows.Forms.Button bt_sub;
        private System.Windows.Forms.Button bt_mul;
        private System.Windows.Forms.Button bt_div;
        private System.Windows.Forms.Button point;
        private System.Windows.Forms.Button ravno;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button bt_sqr;
        private System.Windows.Forms.Button bt_1delx;
        private System.Windows.Forms.Button bt_sqrt;
        private System.Windows.Forms.ListBox history_list;
        private System.Windows.Forms.Button clear_history;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem режимToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обычныйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem расширенныйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
    }
}

