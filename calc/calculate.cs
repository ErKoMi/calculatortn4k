﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calc
{
    public class calculate
    {
        double a, b;
        Operations op;
        bool f_first = false;
        bool f_second = false;
        string str;

        public double A
        {
            get { return a; }
            set
            {
                a = value;
                f_first = true;
            }
        }
        public double B
        {
            get { return b; }
            set
            {
                b = value;
                f_second = true;
            }
        }
        public Operations Op { get => op; private set => op = value; }


        public enum Operations // перечисление операций
        {
            none,
            sum,
            sub,
            mul,
            div,
            sqr,
            fr,
            sqrt
        }

        private double Cal() // Метод вычисления
        {
            str += A;
            switch (op)
            {
                case Operations.sum:
                    A += B;
                    //str += "+";
                    break;
                case Operations.sub:
                    A -= B;
                    //str += "-";
                    break;
                case Operations.mul:
                    A *= B;
                    //str += "*";
                    break;
                case Operations.div:
                    if (B != 0)
                    {
                        A /= B;
                        //str += "/";
                    }
                    else throw new DivideByZeroException();
                    break;

                case Operations.sqr:
                    {
                        A = A * A;
                        break;
                    }
                case Operations.fr:
                    {
                        if (A != 0)
                        {
                            A = 1/A;
                        }
                        else throw new DivideByZeroException();
                        break;
                    }
                case Operations.sqrt:
                    {
                        A = Math.Sqrt(A);
                        break;
                    }
                default:
                    break;
            }
            str += B;
            return A;
        }

        public string Inp_op(double v, Operations op, bool change) //znaki, v - текущее значение на табло, Operations op - операции
        {
            if (!change) // если число не было изменено
            {
                f_second = false; // ожидается ввод второго операнда
            }
            else
            {
                if (!f_second && f_first) // если не считан второй операнд и считан первый
                {
                    B = v; // запоминаем 2 операнд
                    Cal(); // производим вычисление
                    f_second = false; // ожидается ввод второго операнда
                }

                if (!f_first) // если не считан первый операнд
                {
                    A = v; // запоминаем 1 операнд
                }


            }

            Op = op; // запоминаем математическую операцию
            return A.ToString(); // выводим результат
        }

        public string Out(double v) //ravno
        {
            if (Op == Operations.none) // если операция не считана
            {
                A = v;
                return A.ToString();
            }

            if (!f_second)
            {
                B = v;
            }


            return Cal().ToString(); // возвращаем результат операции
        }
        public void ChangeSign() // изменяется знак первого операнда
        {
            A = -A;
        }
        public void Sbros()
        {
            A = B = 0;
            f_first = false;
            f_second = false;
            Op = Operations.none; // сброс операции и операндов
        }
    }
}
